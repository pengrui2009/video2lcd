/******************************************************************************
	项目名称	：SGE800计量智能终端平台
	文件		：typedef.h
	描述		：本文件定义了平台采用的数据类型
	版本		：1.0
	作者		：万鸿俊
	创建日期	：2009.09
******************************************************************************/

#ifndef _TYPEDEF_H
#define _TYPEDEF_H


typedef signed char s8;
typedef unsigned char u8;
typedef signed short s16;
typedef unsigned short u16;
typedef signed int s32;
typedef unsigned int u32;
typedef signed long long int s64;
typedef unsigned long long int u64;

typedef float f32;
typedef double f64;

typedef u16 __bitwise be16;
typedef u16 __bitwise le16;
typedef u32 __bitwise be32;
typedef u32 __bitwise le32;
typedef u64 __bitwise be64;
typedef u64 __bitwise le64;

#define TRUE  1
#define FALSE 0

#endif   /* _TYPEDEF_H */
